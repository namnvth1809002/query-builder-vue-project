import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import VueJsx from '@vitejs/plugin-vue-jsx'
import VueTypeImports from 'vite-plugin-vue-type-imports'
import AutoImport from 'unplugin-auto-import/vite'
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers'
import Components from 'unplugin-vue-components/vite'
import Pages from 'vite-plugin-pages'
import Unocss from 'unocss/vite'
import { presetAttributify, presetUno } from 'unocss'
import presetIcons from '@unocss/preset-icons'
import presetWebFonts from '@unocss/preset-web-fonts'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VueJsx(),
    VueTypeImports(),

    // https://github.com/hannoeru/vite-plugin-pages
    Pages({
      extensions: ['vue'],
      exclude: ['**/components/**/*'],
    }),
    // https://github.com/antfu/unplugin-auto-import
      AutoImport({
        imports: [
          'vue',
          'vue-router',
          '@vueuse/core',
          'pinia',
          {
            'naive-ui': [
              'useMessage',
              'useDialog',
              'useNotification',
            ],
            // 'vue-i18n': [
            //   'useI18n',
            // ],
          },
        ],
        dts: true,
      }),

       // https://github.com/antfu/vite-plugin-components
      Components({
        resolvers: [
          NaiveUiResolver(),
          // auto import icons
          // https://github.com/antfu/vite-plugin-icons
          // IconsResolver({
          //   componentPrefix: '',
          // }),
        ],
        dts: true,
      }),

       // https://github.com/antfu/unocss
      Unocss({
        shortcuts: [
          ['btn', 'px-4 py-1 rounded inline-block bg-teal-600 text-white cursor-pointer hover:bg-teal-700 disabled:cursor-default disabled:bg-gray-600 disabled:opacity-50'],
          ['icon-btn', 'text-[0.9em] inline-block cursor-pointer select-none opacity-75 transition duration-200 ease-in-out hover:opacity-100 hover:text-teal-600'],
          ['wh-full', 'w-full h-full'],
          ['center-layout', 'w-1280px mx-auto px-15px'],
          ['flex-center', 'flex justify-center items-center'],
          ['flex-col-center', 'flex-center flex-col'],
          ['flex-x-center', 'flex justify-center'],
          ['flex-y-center', 'flex items-center'],
          ['inline-flex-center', 'inline-flex justify-center items-center'],
          ['inline-flex-x-center', 'inline-flex justify-center'],
          ['inline-flex-y-center', 'inline-flex items-center'],
          ['flex-col-stretch', 'flex flex-col items-stretch'],
          ['inline-flex-col-stretch', 'flex flex-col items-stretch'],
          ['flex-1-hidden', 'flex-1 overflow-hidden'],
          ['absolute-center', 'absolute left-0 top-0 flex-center wh-full'],
          ['absolute-lt', 'absolute left-0 top-0'],
          ['absolute-lb', 'absolute left-0 bottom-0'],
          ['absolute-rt', 'absolute right-0 top-0'],
          ['absolute-rb', 'absolute right-0 bottom-0'],
          ['fixed-center', 'fixed left-0 top-0 flex-center wh-full'],
          ['nowrap-hidden', 'whitespace-nowrap overflow-hidden'],
          ['ellipsis-text', 'nowrap-hidden overflow-ellipsis'],
        ],
      presets: [
          presetUno(),
          presetAttributify(),
          presetIcons({
            scale: 1.0,
            extraProperties: {
            },
          }),
          presetWebFonts({
            provider: 'google',
            fonts: {
              sans: 'Roboto',
            },
          }),
        ],
        theme: {
          colors: {
            'primary': 'var(--primary-color)',
            'primary-hover': 'var(--primary-color-hover)',
            'primary-pressed': 'var(--primary-color-pressed)',
            'primary-active': 'var(--primary-color-active)',
            'info': 'var(--info-color)',
            'info-hover': 'var(--info-color-hover)',
            'info-pressed': 'var(--info-color-pressed)',
            'info-active': 'var(--info-color-active)',
            'success': 'var(--success-color)',
            'success-hover': 'var(--success-color-hover)',
            'success-pressed': 'var(--success-color-pressed)',
            'success-active': 'var(--success-color-active)',
            'warning': 'var(--warning-color)',
            'warning-hover': 'var(--warning-color-hover)',
            'warning-pressed': 'var(--warning-color-pressed)',
            'warning-active': 'var(--warning-color-active)',
            'error': 'var(--error-color)',
            'error-hover': 'var(--error-color-hover)',
            'error-pressed': 'var(--error-color-pressed)',
            'error-active': 'var(--error-color-active)',
            'light': '#ffffff',
            'dark': '#18181c',
            'deep-dark': '#101014',
          },
          breakpoints: {
            '3xl': '1792px',
            '4xl': '2048px',
          },
        },
      }),
  ],

  server: {
    port: 3333,
    open: true,
    fs: {
      strict: true,
    },
  },

   optimizeDeps: {
      include: [
        'vue',
        'vue-router',
        '@vueuse/core',
      ],
      exclude: [
        'vue-demi',
      ],
    },
})
